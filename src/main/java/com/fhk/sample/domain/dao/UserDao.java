package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.UserBean;

public interface UserDao extends JpaRepository<UserBean, Long>
{
	UserBean findByUsername( String username );
}
