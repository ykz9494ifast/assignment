package com.fhk.sample.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fhk.sample.domain.entity.BookBean;

public interface BookDao extends JpaRepository<BookBean, Long>
{

}
