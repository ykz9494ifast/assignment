package com.fhk.sample.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="BOOK")
public class BookBean implements Serializable 
{
	private static final long serialVersionUID = -4126788655432292921L;
	

	@SequenceGenerator(name="BOOK_SEQ", sequenceName="BOOK_SEQ", allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BOOK_SEQ")
	@Column(name="BOOK_ID")
	private Long bookId;
	
	@Column(name="SUBJECT")
	private String subject;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="ISBN")
	private String isbn;
	
	@Column(name="CONTENT")
	private String content;
	
	@Column(name="CONTENT_TYPE")
	private String contentType;
	
	@Column(name="AUTHOR_ID")
	private Long authorId;
	
	@ManyToOne()
	@JoinColumn(name = "AUTHOR_ID", referencedColumnName = "AUTHOR_ID", insertable = false, updatable = false)  
	private AuthorBean author;
	
	public AuthorBean getAuthor() {
		return author;
	}
	
	@Column(name="PUBLISHER_ID")
	private Long publisherId;
	
	@ManyToOne()
	@JoinColumn(name = "PUBLISHER_ID", referencedColumnName = "PUBLISHER_ID", insertable = false, updatable = false)  
	private PublisherBean publisher;
	
	public PublisherBean getPublisherBean() {
		return publisher;
	}
	
	@Column(name="CATEGORY_ID")
	private Long categoryId;
	
	@ManyToOne()
	@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "CATEGORY_ID", insertable = false, updatable = false)  
	private CategoryBean category;
	
	public CategoryBean getCategoryBean() {
		return category;
	}
	
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="PRICE")
	private BigDecimal price;

	
	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookBean other = (BookBean) obj;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		return true;
	}
	
	
	
	
}
