package com.fhk.sample.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="AUTHOR")
public class AuthorBean implements Serializable 
{
	private static final long serialVersionUID = -4126788655432292921L;
	

	@SequenceGenerator(name="AUTHOR_SEQ", sequenceName="AUTHOR_SEQ", allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUTHOR_SEQ")
	@Column(name="AUTHOR_ID")
	private Long authorId;

	@Column(name="NAME")
	private String name;
	
	@Column(name="COUNTRY")
	private String country;
	
	@Column(name="EMAIL")
	private String email;
	
	@OneToMany(targetEntity=BookBean.class, mappedBy="author",cascade=CascadeType.ALL, fetch = FetchType.LAZY)    
	private List<BookBean> bookbean = new ArrayList<>();

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorBean other = (AuthorBean) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		return true;
	}
	
	
	
	
}
