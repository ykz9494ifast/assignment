package com.fhk.sample.service;

import java.util.List;

import com.fhk.sample.domain.dto.UserDto;
import com.fhk.sample.domain.entity.UserBean;

public interface UserService
{
	void addUser(final UserDto user);
	void updateUser(final UserBean user);
	void deleteUserByUserId(final Long userId);
	
	UserBean getByUsername( String username );
	
	List<UserBean> findUserAll();

}
