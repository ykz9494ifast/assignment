package com.fhk.sample.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.AuthorDao;
import com.fhk.sample.domain.dao.BookDao;
import com.fhk.sample.domain.entity.BookBean;
import com.fhk.sample.service.BookService;

@Service
class BookServiceImpl implements BookService
{
	@Inject
	private BookDao bookDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<BookBean> findBookAll() 
	{
		return bookDao.findAll();
	}
	
}
