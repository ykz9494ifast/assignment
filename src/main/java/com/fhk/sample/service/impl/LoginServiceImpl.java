package com.fhk.sample.service.impl;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.LoginService;
import com.fhk.sample.service.UserService;

@Service
public class LoginServiceImpl implements LoginService, UserDetailsService
{
	@Inject 
	private UserService userService;

	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("cvvvvvvvvvvvasdasdsadvvvvvvvvvvvvvv" + userService == null );
		UserBean user = userService.getByUsername(username);
    	System.out.println("asdasdasdasdsad" + username + "isempty");
        if (user == null) {
        	System.out.println("ISNUILL");
        	throw new UsernameNotFoundException(username);
        }
        else
        {
        	System.out.println("NOTNULLL");
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        
        grantedAuthorities.add(new SimpleGrantedAuthority("User")); //Hardcoded role
        grantedAuthorities.add(new SimpleGrantedAuthority("Moderator")); //Hardcoded role
        System.out.println("dddddddddddd");
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}
