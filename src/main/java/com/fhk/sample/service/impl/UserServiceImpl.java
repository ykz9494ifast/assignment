package com.fhk.sample.service.impl;

import java.util.Date;
import java.util.List;


import javax.inject.Inject;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhk.sample.domain.dao.UserDao;
import com.fhk.sample.domain.dto.UserDto;
import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.UserService;

@Service
public class UserServiceImpl implements UserService
{
	@Inject
	private UserDao userDao;

	@Override
	@Transactional(readOnly=true)
	public List<UserBean> findUserAll() 
	{
		return userDao.findAll();
	}
//
//
//	@Override
//	@Transactional
//    public void registerNewUserAccount(UserBean userBean) throws UserAlreadyExistException {
//        if (emailExists(userDto.getEmail())) {
//            throw new UserAlreadyExistException("There is an account with that email address: "
//              + userDto.getEmail());
//        }
//
//        User user = new User();
//        user.setFirstName(userDto.getFirstName());
//        user.setLastName(userDto.getLastName());
//        user.setPassword(userDto.getPassword());
//        user.setEmail(userDto.getEmail());
//        user.setRoles(Arrays.asList("ROLE_USER"));
//
//        return repository.save(user);
//    }
	
	public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@Override
	@Transactional
	public void addUser(final UserDto user) 
	{

		System.out.println("ffffffffffffsssssssfffffffffffff:");
		UserBean u = new UserBean();
	
		u.setUsername(user.getUsername());
		u.setPassword(passwordEncoder().encode(user.getPassword()));
		u.setName(user.getName());
		u.setEmail(user.getEmail());
		u.setTelephone(user.getTelephone());
		u.setMobile(user.getMobile());
		u.setAddress(user.getAddress());
		u.setCreatedDate(new Date());
		u.setStatus("Pending");
		System.out.println("fffffffffffffffffffffffff:" + u.getUsername() + user.getMobile());
		userDao.save(u);
	}

	@Override
	@Transactional
	public void updateUser(final UserBean user) 
	{
		userDao.save(user);
	}

	@Override
	@Transactional
	public void deleteUserByUserId(final Long userId) 
	{
		userDao.delete(userId);
	}	
	
	@Override
	@Transactional
	public UserBean getByUsername(final String username) 
	{
		UserBean ub = userDao.findByUsername(username);
		System.out.println("USERSERVICE" + ub == null);
		return userDao.findByUsername( username );
	}	
//	
//	@Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//		System.out.println("cvvvvvvvvvvvasdasdsadvvvvvvvvvvvvvv"  );
//		UserBean user = userDao.findByUsername(username);
//    	System.out.println("asdasdasdasdsad");
//        if (user == null) {
//        	System.out.println("ISNUILL");
//        	throw new UsernameNotFoundException(username);
//        }
//        else
//        {
//        	System.out.println("NOTNULLL");
//        }
//
//        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//        
//        grantedAuthorities.add(new SimpleGrantedAuthority("User")); //Hardcoded role
//        grantedAuthorities.add(new SimpleGrantedAuthority("Moderator")); //Hardcoded role
//        System.out.println("dddddddddddd");
//        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
//    }
}
