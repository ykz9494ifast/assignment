package com.fhk.sample.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.dto.UserDto;
import com.fhk.sample.domain.entity.TestBean;
import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.UserService;

@RestController
@RequestMapping(value = "/rest/user")
public class UserController 
{
	@Inject
	private UserService userService;
//	
//	 @RequestMapping(value = "/user/", method = RequestMethod.POST)
//    public ResponseEntity<Void> createUser(@RequestBody User user,    UriComponentsBuilder ucBuilder) {
//        System.out.println("Creating User " + user.getUsername());
//  
//        if (userService.isUserExist(user)) {
//            System.out.println("A User with name " + user.getUsername() + " already exist");
//            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
//        }
//  
//        userService.saveUser(user);
//  
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
//    }
	 
    @RequestMapping(value = "add-user", method = RequestMethod.POST)
	public Map<String,String> createUser( @RequestBody UserDto user )
	{
    	System.out.println("sssssssssssssssssssssss");
		userService.addUser(user);
		
//		HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
		return new HashMap<String,String>();
	}
}
