package com.fhk.sample.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.*;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.dto.UserDto;
import com.fhk.sample.domain.entity.TestBean;
import com.fhk.sample.domain.entity.UserBean;
import com.fhk.sample.service.LoginService;
import com.fhk.sample.service.UserService;

@RestController
@RequestMapping(value = "/rest/user")
public class LoginController 
{
//	@Inject
//	private LoginService loginService;
	
	@RequestMapping(value = "login")
    public String login(Model model, String error, String logout) {
		System.out.println("LoginController");
        if (error != null)
            model.addAttribute("errorMsg", "Your username and password are invalid.");

        if (logout != null)
            model.addAttribute("msg", "You have been logged out successfully.");

        return "login";
    }
}
