package com.fhk.sample.web.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fhk.sample.domain.entity.BookBean;
import com.fhk.sample.service.BookService;

@RestController
@RequestMapping(value = "/rest/booklist")
public class BookController 
{
	@Inject
	private BookService bookService;
	
	@RequestMapping(value = "find-book-all", method = RequestMethod.POST)
	public List<BookBean> findBookAll() 
	{
		return bookService.findBookAll();
	}
	
	@RequestMapping(value = "get-book-all", method = RequestMethod.GET)
	public List<BookBean> getBookAll() 
	{
		return bookService.findBookAll();
	}
	
}
