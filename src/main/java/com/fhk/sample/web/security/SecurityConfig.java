package com.fhk.sample.web.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fhk.sample.service.UserService;
import com.fhk.sample.service.impl.UserServiceImpl;
import com.fhk.sample.service.impl.LoginServiceImpl;

@ConditionalOnWebApplication
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{
	
//    @Override
//    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//            .withUser("user1").password("user1Pass").roles("USER")
//            .and()
//            .withUser("user2").password("user2Pass").roles("USER")
//            .and()
//            .withUser("admin").password("adminPass").roles("ADMIN");
//    }
	
	@Override
	protected void configure(final HttpSecurity http) throws Exception 
	{
//		http.authorizeRequests().anyRequest().permitAll()
//		.and().csrf().disable();
		http
        .authorizeRequests()
        .antMatchers("/main",
        		"/main#/home",
        		"/login**",
        		"/perform_login",
        		"/resources/**",
        		"/css/**",
        		"/images/**",
        		"/js/**",
        		"/std/**",
        		"/system/**",
        		"/favicon.ico",
        		"/rest/**",
        		"/rest/user/**" ).permitAll()
        .anyRequest().authenticated()
        .and().csrf().disable()
        .formLogin()
		.loginPage("/main#/login").permitAll()
		.loginProcessingUrl("/perform_login").permitAll()
		.defaultSuccessUrl("/main#/home").permitAll();
//        .anyRequest().authenticated()
//        .and().csrf().disable()
//        .formLogin();
//		
		
		
		
//	      .loginPage("/main#/login")
//	      .permitAll();
//        .and().csrf().disable()
//		.and().formLogin()
//		.loginPage("/main#/login")
//		.permitAll()
//        .and()
//        .logout()
//        .permitAll();
//        .and()
//        .formLogin()
//        .loginPage("/login")
//        .permitAll()
//        .and()
//        .logout()
//        .invalidateHttpSession(true)
//        .clearAuthentication(true)
//        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//        .logoutSuccessUrl("/login?logout")
//        .permitAll();
		
//		http.authorizeRequests().anyRequest().permitAll()
//		.and().csrf().disable();
//		.formLogin()
//		.loginPage("/login");
	}

	@Bean
    public UserDetailsService userDetailsService() {
        return new LoginServiceImpl();
    }

	@Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService());
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }
	
	@Override
	public void configure(final AuthenticationManagerBuilder auth) throws Exception 
	{
//		auth.inMemoryAuthentication().withUser("admin").password("admin")
//				.roles("ADMIN", "USER").and().withUser("user").password("user")
//				.roles("USER");
		auth.authenticationProvider(authenticationProvider());
	}


}
