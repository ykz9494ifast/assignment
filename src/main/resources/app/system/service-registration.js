"use strict";
(function () {
  var serviceApp = angular.module("sample.service");
  serviceApp.factory("registrationService", [
    "$log",
    "$http",
    RegistrationService,
  ]);

  function RegistrationService($log, $http) {
    var ret = {};

    ret.addUser = function (user) {
      console.log("*******************************");
      return $http.post(_contextPath + "/rest/user/add-user", user);
    };

    return ret;
  }
})();
