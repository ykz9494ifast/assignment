"use strict";
(function () {
  var serviceApp = angular.module("sample.service");
  serviceApp.factory("loginService", ["$log", "$http", LoginService]);

  function LoginService($log, $http) {
    var ret = {};

    ret.login = function (user) {
      console.log(">>>>>>>>>>>>>>>>>", user);
      return $http.post(_contextPath + "/rest/user/login", user);
    };

    return ret;
  }
})();
