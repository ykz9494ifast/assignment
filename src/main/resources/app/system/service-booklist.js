"use strict";
(function () {
  var serviceApp = angular.module("sample.service");
  serviceApp.factory("booklistService", ["$log", "$http", BooklistService]);

  function BooklistService($log, $http) {
    var ret = {};

    ret.findBookAll = function()
 	   	{
 	   	   	return $http({
 				method : 'POST',
 				url: _contextPath + '/rest/booklist/find-book-all',
 			});
 	   	};

    return ret;
  }
})();
