'use strict';
(function()
{
	var app = angular.module('std.app');
	app.controller('BooklistController', ['$scope', 'booklistService', BooklistController]);
	
	function BooklistController($scope, booklistService)
	{
		$scope.sort = {
			column: '',
			descending: false
		};    
		
		$scope.changeSorting = function(column) {
	
			var sort = $scope.sort;
	
			if (sort.column == column) {
				sort.descending = !sort.descending;
			} else {
				sort.column = column;
				sort.descending = false;
			}
		};
			
		$scope.selectedCls = function(column) {
			return column == $scope.sort.column && 'sort-' + $scope.sort.descending;
		};

		$scope.refresh = function()
		{
			booklistService.findBookAll().then(function(result)
			{
				$scope.data = result.data;
			});
		};

		
		$scope.refresh();
	}
	
	                                      
})();
