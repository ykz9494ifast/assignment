"use strict";
(function () {
  var app = angular.module("std.app");
  app.controller("LoginController", [
    "$scope",
    "loginService",
    LoginController,
  ]);

  function LoginController($scope, loginService) {
    $scope.user = {
      username: "",
      password: "",
    };
    // function createUser(user){
    // 	registrationService.addUser(user);
    // }

    // function submit() {
    // 	if(self.user.id===null){
    // 		createUser(self.user);
    // 	}else{
    // 		updateUser(self.user, self.user.id);
    // 		console.log('User updated with id ', self.user.id);
    // 	}
    // 	reset();
    // }

    $scope.login = function (user) {
      console.log(">>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<");
      loginService.login(user).then(function (result) {
        $scope.refresh();
      });
    };
  }
})();
