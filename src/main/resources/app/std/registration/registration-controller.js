"use strict";
(function () {
  var app = angular.module("std.app");
  app.controller("RegistrationController", [
    "$scope",
    "registrationService",
    RegistrationController,
  ]);

  function RegistrationController($scope, registrationService) {
    $scope.user = {
      username: "",
      password: "",
      name: "",
      email: "",
      telephone: "",
      mobile: "",
      address: "",
    };
    // function createUser(user){
    // 	registrationService.addUser(user);
    // }

    // function submit() {
    // 	if(self.user.id===null){
    // 		createUser(self.user);
    // 	}else{
    // 		updateUser(self.user, self.user.id);
    // 		console.log('User updated with id ', self.user.id);
    // 	}
    // 	reset();
    // }

    $scope.createUser = function (user) {
      console.log(">>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<");
      registrationService.addUser(user).then(function (result) {
      });
    };
  }
})();
